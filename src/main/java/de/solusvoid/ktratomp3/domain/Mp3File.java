package de.solusvoid.ktratomp3.domain;

import de.solusvoid.ktratomp3.KtraToMp3Application;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

@Data
@Log4j2
public class Mp3File implements Runnable {
    @Override
    public void run() {

        /* Download MP3 */
        long startTime = System.currentTimeMillis();
        long duration;
        Integer downloadMp3File = KtraToMp3Application.MP3_DOWNLOAD_QUEUE.poll();
        if (downloadMp3File != null) {

            try {
                log.info("Starting mp3File download for episode: {}. Thread id: {}", downloadMp3File, Thread.currentThread().getId());
                if (downloadMp3File < 10) {
                    FileUtils.copyURLToFile(new URL(KtraToMp3Application.KTRA_MP3_BASE_URL + "00" + downloadMp3File + ".mp3"),
                            new File(KtraToMp3Application.outputDir, "KTRA_Episode_00" + downloadMp3File + ".mp3"));
                } else if (downloadMp3File < 100) {
                    FileUtils.copyURLToFile(new URL(KtraToMp3Application.KTRA_MP3_BASE_URL + "0" + downloadMp3File + ".mp3"),
                            new File(KtraToMp3Application.outputDir, "KTRA_Episode_0" + downloadMp3File + ".mp3"));
                } else {
                    FileUtils.copyURLToFile(new URL(KtraToMp3Application.KTRA_MP3_BASE_URL + downloadMp3File + ".mp3"),
                            new File(KtraToMp3Application.outputDir, "KTRA_Episode_" + downloadMp3File + ".mp3"));
                }

                duration = (System.currentTimeMillis() - startTime) / 1000;
                log.info("Finished mp3File download for episode: {} in {} seconds. Thread id: {}", downloadMp3File, duration, Thread.currentThread().getId());
            } catch (IOException e) {
                log.error("Failed to create URL", e);
            }

        }

    }
}
