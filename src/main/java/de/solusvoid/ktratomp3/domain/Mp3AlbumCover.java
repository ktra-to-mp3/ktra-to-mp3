package de.solusvoid.ktratomp3.domain;

import de.solusvoid.ktratomp3.KtraToMp3Application;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.net.URL;

@Data
@Log4j2
public class Mp3AlbumCover implements Runnable {
    private int episodeNumber;
    private File image;

    @Override
    public void run() {

        /* Download Album Cover */
        long startTime = System.currentTimeMillis();
        long duration;
        Document albumCoverDocument = null;
        Mp3AlbumCover mp3AlbumCover = new Mp3AlbumCover();
        Integer episodeDownload = KtraToMp3Application.ALBUM_COVER_DOWNLOAD_QUEUE.poll();

        if (episodeDownload != null) {

            try {

                log.info("Starting mp3AlbumCover download for episode: {}. Thread id: {}", episodeDownload, Thread.currentThread().getId());

                albumCoverDocument = Jsoup.connect(KtraToMp3Application.KTRA_METADATA_BASE_URL + episodeDownload).get();

                Elements albumCover = albumCoverDocument.select("img");

                FileUtils.copyURLToFile(new URL(albumCover.get(2).attributes().get("src")),
                        new File(KtraToMp3Application.outputDir + File.separator + "albumcover" + File.separator + "KTRA_Album_Cover_" + episodeDownload + ".jpeg"));

                mp3AlbumCover.setImage(new File(KtraToMp3Application.outputDir + File.separator + "albumcover" + File.separator + "KTRA_Album_Cover_" + episodeDownload + ".jpeg"));


                duration = (System.currentTimeMillis() - startTime) / 1000;
                log.info("Finished mp3AlbumCover download for episode {} in {} seconds. Thread id: {}", episodeDownload, duration, Thread.currentThread().getId());

            } catch (IOException e) {
                log.error("Failed to get jsoup object", e);
            }
        }
    }
}
