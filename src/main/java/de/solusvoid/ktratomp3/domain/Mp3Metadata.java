package de.solusvoid.ktratomp3.domain;

import de.solusvoid.ktratomp3.KtraToMp3Application;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.json.JSONWriter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;


@Data
@Log4j2
public class Mp3Metadata implements Runnable {
    private LinkedList<String> trackList;
    private int episodeNumber;
    private String guestDj;
    private String mp3Filename;
    private String description;

    @Override
    public void run() {
        /* Download Metadata */
        long startTime = System.currentTimeMillis();
        long duration;
        Document metadataDocument = null;
        Mp3Metadata mp3Metadata = new Mp3Metadata();
        LinkedList<String> rawTrackList = new LinkedList<>();
        Integer downloadMetadata = KtraToMp3Application.METADATA_DOWNLOAD_QUEUE.poll();

        LinkedList<String> rawMetaData = new LinkedList<>();

        if (downloadMetadata != null) {
            try {
                episodeNumber = downloadMetadata;
                log.info("Starting mp3Metadata download for episode: {}. Thread id: {}", downloadMetadata, Thread.currentThread().getId());
                metadataDocument = Jsoup.connect(KtraToMp3Application.KTRA_METADATA_BASE_URL + downloadMetadata).get();

                Elements metadataElements = metadataDocument.select("p");
                Elements headerElements = metadataDocument.select("h1");

                String header = headerElements.textNodes().get(0).getWholeText();

                List<TextNode> metadataString = metadataElements.textNodes();

                for (TextNode textNode : metadataString) {
                    if (!textNode.isBlank() && !textNode.toString().equals("&nbsp;")) {
                        rawMetaData.add(textNode.getWholeText());
                    }
                }

                for (int i = 1; i < rawMetaData.size(); i++) {
                    rawTrackList.add(rawMetaData.get(i));
                }


                guestDj = header.substring(header.indexOf(": ") + 2);
                mp3Filename = "ktra_" + header.toLowerCase().replace(' ', '_').replace(":", "");
                description = rawMetaData.get(0);
                this.trackList = rawTrackList;

                mp3Metadata.setGuestDj(guestDj);
                mp3Metadata.setMp3Filename(mp3Filename);
                mp3Metadata.setDescription(description);
                mp3Metadata.setTrackList(this.trackList);
                mp3Metadata.setEpisodeNumber(episodeNumber);

                duration = (System.currentTimeMillis() - startTime) / 1000;
                log.info("Finished mp3Metadata download for episode: {} in {} seconds . Thread id: {}", episodeNumber, duration, Thread.currentThread().getId());

                generateJsonMetadataFile();
            } catch (IOException e) {
                log.error("Failed to get jsoup object", e);
            }
        }
    }

    private void generateJsonMetadataFile() {

        log.info("Start writing metadata for episode: {} to JSON file", episodeNumber);

        File file = new File(KtraToMp3Application.outputDir + File.separator + "metadata");
        file.mkdirs();

        try (FileWriter fileWriter = new FileWriter(file.getAbsolutePath() + File.separator + "episode_" + episodeNumber + ".json", StandardCharsets.UTF_8)) {

            new JSONWriter(fileWriter)
                    .object()
                    .key("episodeNumber")
                    .value(episodeNumber)
                    .key("guestDj")
                    .value(guestDj)
                    .key("mp3Filename")
                    .value(mp3Filename)
                    .key("description")
                    .value(description)
                    .key("tracklist")
                    .value(String.join(",", trackList))
                    .endObject();

            log.info("Finished writing metadata for episode: {} to JSON file", episodeNumber);
        } catch (IOException e) {
            log.error("Failed to write json metadata file for episode: {}", episodeNumber, e);
        }
    }
}
