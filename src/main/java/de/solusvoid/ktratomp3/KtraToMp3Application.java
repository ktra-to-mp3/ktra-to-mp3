package de.solusvoid.ktratomp3;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.cli.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

@SpringBootApplication
@Log4j2
public class KtraToMp3Application {

    /* http://www.djkutski.com/podcast/keepingtheravealive001.mp3 */
    public static final String KTRA_MP3_BASE_URL = "https://www.djkutski.com/podcast/keepingtheravealive";
    /* https://www.keepingtheravealive.com/podcast/episode-1 */
    public static final String KTRA_METADATA_BASE_URL = "https://www.keepingtheravealive.com/podcast/episode-";
    private static final Options options = new Options();
    public static final BlockingQueue<Integer> MP3_DOWNLOAD_QUEUE = new LinkedBlockingQueue<>();
    public static final BlockingQueue<Integer> METADATA_DOWNLOAD_QUEUE = new LinkedBlockingQueue<>();
    public static final BlockingQueue<Integer> ALBUM_COVER_DOWNLOAD_QUEUE = new LinkedBlockingQueue<>();
    public static String outputDir;
    private static boolean downloadPodcast = true;
    private static boolean downloadAlbumCover = true;
    private static boolean downloadMetadata = true;
    private static int maximumNumberOfThreadsForDownload;
    private static int startDownloadRange;
    private static int endDownloadRange;

    public static void main(String[] args) {
        SpringApplication.run(KtraToMp3Application.class, args);

        /* Set command line options */
        setCommandLineOptions();

        /* Read command line arguments */
        readCommandLineOptions(args);

        /* Generate download queue with command line params */
        generateQueues(startDownloadRange, endDownloadRange);

        log.info("Initializing fixed thread pool");
        ExecutorService executor = Executors.newFixedThreadPool(maximumNumberOfThreadsForDownload);
        log.info("Finished initializing fixed thread pool with: {} threads.", maximumNumberOfThreadsForDownload);

        for (int i = startDownloadRange; i <= endDownloadRange; i++) {

            if (!MP3_DOWNLOAD_QUEUE.isEmpty() && downloadPodcast) {
                log.info("Starting mp3File thread");
                executor.execute(new de.solusvoid.ktratomp3.domain.Mp3File());
                log.info("Started mp3File thread");
            }

            if (!METADATA_DOWNLOAD_QUEUE.isEmpty() && downloadMetadata) {
                log.info("Starting mp3Metadata thread");
                executor.execute(new de.solusvoid.ktratomp3.domain.Mp3Metadata());
                log.info("Started mp3Metadata thread");
            }

            if (!ALBUM_COVER_DOWNLOAD_QUEUE.isEmpty() && downloadAlbumCover) {
                log.info("Starting mp3AlbumCover thread");
                executor.execute(new de.solusvoid.ktratomp3.domain.Mp3AlbumCover());
                log.info("Started mp3AlbumCover thread");
            }

        }

        log.info("Shutting down executor service");
        executor.shutdown();
        log.info("Shut down executor service");

    }

    private static void generateQueues(int downloadFrom, int downloadTill) {
        for (int i = downloadFrom; i <= downloadTill; i++) {
            try {
                MP3_DOWNLOAD_QUEUE.put(i);
                METADATA_DOWNLOAD_QUEUE.put(i);
                ALBUM_COVER_DOWNLOAD_QUEUE.put(i);
            } catch (InterruptedException e) {
                log.error("Failed to put number in queue. The following exception was thrown: ", e);
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * Configures all available command line options
     */
    private static void setCommandLineOptions() {
        options.addRequiredOption("s", "start", true,
                "Start of download range");
        options.addRequiredOption("e", "end", true,
                "End of download range");
        options.addOption("o", "output-dir", true,
                "Output directory for the files");
        options.addOption("t", "threads", true,
                "Number of threads to use for downloading");

        Option downloadOption = new Option("d", "download", true,
                "What should be excluded from the download? p = podcast, c = cover, m = metadata. \n" +
                        "By default nothing will be ignored");
        downloadOption.setArgs(Option.UNLIMITED_VALUES);

        options.addOption(downloadOption);
    }


    /**
     * Parses command line options
     *
     * @param args Application launch option passed on from the main method
     */
    private static void readCommandLineOptions(String[] args) {
        CommandLineParser commandLineParser = new DefaultParser();
        try {
            CommandLine commandLine = commandLineParser.parse(KtraToMp3Application.options, args);
            if (commandLine.hasOption("s")) {
                startDownloadRange = Integer.parseInt(commandLine.getOptionValue("s"));
            }

            if (commandLine.hasOption("e")) {
                endDownloadRange = Integer.parseInt(commandLine.getOptionValue("e"));
            }

            if (commandLine.hasOption("o")) {
                outputDir = commandLine.getOptionValue("o");
            } else {
                outputDir = "src/main/podcasts";
            }

            if (commandLine.hasOption("t")) {
                maximumNumberOfThreadsForDownload = Integer.parseInt(commandLine.getOptionValue("t"));
            } else {
                maximumNumberOfThreadsForDownload = 3;
            }

            if (commandLine.hasOption("d")) {
                for (String string : commandLine.getOptionValues("d")) {

                    if (string.equals("p")) {
                        downloadPodcast = false;
                    }

                    if (string.equals("c")) {
                        downloadAlbumCover = false;
                    }

                    if (string.equals("m")) {
                        downloadMetadata = false;
                    }
                }
            }
        } catch (ParseException e) {
            log.error("Failed to parse command line option.", e);
        }
    }
}
